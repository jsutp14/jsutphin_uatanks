﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    //References
    GameManager game;

    public bool singlePlayer = false;
    public bool splitScreen = false;
    public bool menu = false;
    public bool settingsSelected = false;
    public bool exitGame = false;
    public bool restart = false;


    // Start is called before the first frame update
    void Start()
    {
        game = GetComponent<GameManager>();    
    }

    public void SinglePlayer()
    {
        singlePlayer = true;
        //SceneManager handles the scenes that are loaded within the build settings. The build index directly references these in numerical order
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        //This references the Audio manager, and gives the buttons the click sound byte
        SFXManager.sfxInstance.SFX.PlayOneShot(SFXManager.sfxInstance.Click);
    }

    public void TwoPlayer()
    {
        splitScreen = true;
        //SceneManager handles the scenes that are loaded within the build settings. The build index directly references these in numerical order
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);

        //This references the Audio manager, and gives the buttons the click sound byte
        SFXManager.sfxInstance.SFX.PlayOneShot(SFXManager.sfxInstance.Click);
    }

    public void Menu()
    {
        menu = true;

        //This references the Audio manager, and gives the buttons the click sound byte
        SFXManager.sfxInstance.SFX.PlayOneShot(SFXManager.sfxInstance.Click);
    }

    public void Settings()
    {
        settingsSelected = true;

        //This references the Audio manager, and gives the buttons the click sound byte
        SFXManager.sfxInstance.SFX.PlayOneShot(SFXManager.sfxInstance.Click);
    }

    public void ExitGame()
    {
        exitGame = true;

        //This references the Audio manager, and gives the buttons the click sound byte
        SFXManager.sfxInstance.SFX.PlayOneShot(SFXManager.sfxInstance.Click);


        //Code is set for game when built, cannot be tested until then. Debug is there for testing purpose
        Debug.Log("Quit");
        Application.Quit();
    }

    public void Restart()
    {
        restart = true;

        //Reloads currently active scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        //This references the Audio manager, and gives the buttons the click sound byte
        SFXManager.sfxInstance.SFX.PlayOneShot(SFXManager.sfxInstance.Click);
    }    

    public void ButtonTest()
    {
        Debug.Log("Button Pressed");
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Use this whenever interacting with UI in code
using UnityEngine.UI;

public class StatUI : MonoBehaviour
{
    //References
    TankData data;
    public Text healthText;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        healthText.text = "HEALTH: " + data.curHealth.ToString();


    }
}

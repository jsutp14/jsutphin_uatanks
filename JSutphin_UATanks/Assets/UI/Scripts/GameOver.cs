﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameOver : MonoBehaviour
{
   //References
    ScoreData score;

    //Variables
    public Text scoreText;

    public void GameOverSetup(int maxPlatform)
    {
        gameObject.SetActive(true);
        scoreText.text = score.ToString() + " POINTS";
    }    


}

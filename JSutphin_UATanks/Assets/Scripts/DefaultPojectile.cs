﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultPojectile : MonoBehaviour
{
    //References
    Rigidbody rb;
    DefaultPojectile defaultPojectile;
    public TankData instigator;
    
    //Variables
    public float damage;
    public float shotSpeed;
    private float destroyProjectile;

    void Start()
    {
        //Initial memory banking of these components
        rb = GetComponent<Rigidbody>();
        defaultPojectile = GetComponent<DefaultPojectile>();
    }

    // Update is called once per frame
    void Update()
    {
        //Every frame that this script is called the projectile has force added forward to its' rigidbody with an impulse forceMode, and a variable from TankData: shotSpeed
        rb.AddForce(transform.forward * shotSpeed, ForceMode.Impulse);
        
        
    }

    public void OnTriggerEnter(Collider other)
    {
        //Defining GameObject class as objectHit
        GameObject objectHit;

        //If this projectile hits another game object
        objectHit = other.gameObject;

        //Defining the Health class as objectHealth
        Health objectHealth;

        //This retrieves the Health component of the other object hit (if applicable)
        objectHealth = objectHit.GetComponent<Health>();

        //If the game object has health
        if (objectHealth != null)
        {
            //The health of the object will be deducted accordingly by the TakeDamage Function
            objectHealth.TakeDamage(damage, instigator);
        }
        //If the collision is true than the object projectile will be destroyed
        if (other == true)
        {
            Destroy(gameObject);
        }
        else
        {
            destroyProjectile = 2;
            destroyProjectile = Time.time + destroyProjectile;

            Destroy(gameObject);
        }    
    }

}

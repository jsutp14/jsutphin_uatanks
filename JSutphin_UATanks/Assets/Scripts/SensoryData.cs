﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensoryData : MonoBehaviour
{
    //References
    AIPreception AI_P;
    AIBehavior AI_B;
    NoiseMaker noise;

    //Variables
    public Transform AIRadius;
    public Transform playerRadius;
    public Transform edge;

    public float shotRadius;
    public float AISenseRadius;
    public float playerSenseRadius;


    // Start is called before the first frame update
    void Start()
    {
        //Initial memory banking of references
        AI_P = GetComponent<AIPreception>();
        noise = GetComponent<NoiseMaker>();

        AISenseRadius = Vector3.Distance(AIRadius.position, edge.position);
        
        playerSenseRadius = Vector3.Distance(playerRadius.position, edge.position);
        shotRadius = Vector3.Distance(playerRadius.position, edge.position);
    }


}

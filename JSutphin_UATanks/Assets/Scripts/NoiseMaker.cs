﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour
{
    //References
    SensoryData sense;
    AIPreception AI_P;
    PlayerAttack attack;
    InputManager inputManager;

    //Variables

    // Start is called before the first frame update
    void Start()
    {
        sense = GetComponent<SensoryData>();
        AI_P = GetComponent<AIPreception>();
        attack = GetComponent<PlayerAttack>();
        inputManager = GetComponent<InputManager>();
    }

    public bool ProducedNoise()
    {

        if (Input.GetKey(KeyCode.Mouse0) == true)
        {
            sense.shotRadius = 50;
        }

        return false;
    }

}

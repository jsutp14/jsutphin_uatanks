﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    TankData data;
    SFXManager SFX;

    // Start is called before the first frame update
    void Start()
    {
        data = GetComponent<TankData>();


    }


    //TakeDamage function that will deduct the health from the damage dealt
    public void TakeDamage(float damage, TankData instigator)
    {
        //The health will now be the health minus the dealt damage
        data.curHealth = data.curHealth - damage;

        //If health is less than or equal to 0, then destroy the game object that health component is attached to
        if (data.curHealth <= 0)
        {
            //Play death sound
            SFX.DeathSound();

            Destroy(gameObject);
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    //References
    public TankData data;
    PlayerMovement motor;
    CharacterController characterController;
    Health health;
    Collectible collectible;
    public GameObject FireRateCollectible;
    public GameObject HealthCollectible;
    public GameObject DamageCollectible;



    //Variables
    public float moveSpeed = 3.0F;
    public float turnSpeed = 3.0F;

    public float shotSpeed = 10;
    public float FireRate = 0.5F;

    //Variables
    public float curHealth;
    public float maxHealth;

    //public float maxHealth = 100;
    public float damage = 10;

    public int maxScore = 10;
    public int allyScore = 0;
    public int enemyScore = 0;
    

    // Start is called before the first frame update
    void Start()
    {
        //Initial memory banking of these components
        motor = GetComponent<PlayerMovement>();
        characterController = GetComponent<CharacterController>();
        health = GetComponent<Health>();
        
    }

    void Update()
    {
        if(FireRateCollectible || HealthCollectible || DamageCollectible == true)
        {
            //collectible.OnTriggerEnter(Collider other);
        }

        if (Input.GetKey(KeyCode.Alpha1))
        {
            health.TakeDamage(damage, data);
        }

        if(curHealth <= 0)
        {
            FindObjectOfType<GameManager>().EndGame();
        }

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
//Non monobehavior class
public class PowerUp
{
    //Variables
    public float speedModifier;
    public float healthModifier;
    public float maxHealthModifier;
    public float fireRateModifier;

    //Duration
    public float duration;

    //Bool
    public bool isPermanent;

    //Modifiers added upon powerup activation
    public void OnActivate(TankData target)
    {

        target.moveSpeed += speedModifier;
        target.curHealth += healthModifier;
        target.maxHealth += maxHealthModifier;
        target.FireRate += fireRateModifier;
    }

    //Modifiers added upon powerup deactivation
    public void OnDeactivate(TankData target)
    {
        target.moveSpeed -= speedModifier;
        target.curHealth -= healthModifier;
        target.maxHealth -= maxHealthModifier;
        target.FireRate -= fireRateModifier;
    }

}

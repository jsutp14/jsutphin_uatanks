﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //References
    TankData data;
    MapGenerator map;
    public static GameManager instance;
    GameObject PlayerTank;
    GameObject EnemyTank;
    public GameObject PlayerSpawn;
    private GameObject spawner;
    PlayerSpawn P_Spawn;
    Buttons buttons;
    GameOver gameOver;

    //Variables
    public float restartDelay;
    public bool gameEnded = false;
    int maxPlatform = 0;
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;


    //Variables
    public float endGameTimer;

    ////Booleans
    //bool GameOver = false;
    //bool destroyPlayer = false;
    //bool destroyEnemy = false;

    // Start is called before the first frame update
    void Start()
    {
        //Initial memory banking of these components
        data = GetComponent<TankData>();
        PlayerTank = GetComponent<GameObject>();
        EnemyTank = GetComponent<GameObject>();
        map = GetComponent<MapGenerator>();
        buttons = GetComponent<Buttons>();
        
        map.GenerateGrid();

        P_Spawn = GetComponent<PlayerSpawn>();

        //if (buttons.singlePlayer == true)
        //{
        //    //new Camera = Camera.GateFitMode.Fill;
        //}

    
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            if(GameIsPaused)
            {
                ResumePlay();
            }
            else
            {
                Pause();
            }
        }
    }

    public void ResumePlay()
    {
        buttons.Menu();

        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;

    }

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void EndGame()
    {
        if (gameEnded == false)
        {
            gameOver.GameOverSetup(maxPlatform);

            gameEnded = true;
            Debug.Log("GAMEOVER");
            Invoke("Restart", restartDelay);
        }
    }



    //void Update()
    //{
    //    //the player is destroyed, the enemy recives a point to their score
    //    if (destroyPlayer == true)
    //    {
    //        data.enemyScore += 1;
    //        //If the enemyScore is equal to the max score than the player looses, and the Gameover boolean is made true
    //        if (data.enemyScore == data.maxScore)
    //        {
    //            GameOver = true;
    //            print("You Lose.");
    //        }
    //    }
    //    //If the allyScore is equal to the max score than the enemy looses, and the Gameover boolean is made true
    //    if (destroyEnemy == true)
    //    {
    //        data.allyScore += 1;
    //        if (data.allyScore == data.maxScore)
    //        {
    //            GameOver = true;
    //            print("You Win!");
    //        }
    //    }
    //    //if Game over is true than end the game
    //    if (GameOver == true)
    //    {
    //        //Timer until the game ends
    //        print(endGameTimer);
    //        endGameTimer = Time.time + endGameTimer;

    //        //End game
    //        Application.Quit();
    //    }

    //}
}

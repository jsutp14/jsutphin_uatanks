﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //References
    private CharacterController characterController;
    public PlayerMovement motor;
    public TankData data;
    //Rigidbody rb;
    
    void Start()
    {
        //Initial memory banking of these components
        characterController = GetComponent<CharacterController>();
        //rb = GetComponent<Rigidbody>();
    }

    //Movement function
    public void Move(Vector3 direction)
    {
        //Using the built in SimpleMove Function attached to the character controller we move in the desired direction and multiply that by the movement speed in tank data
        characterController.SimpleMove(direction * data.moveSpeed);
    }

    //Rotation function
    public void Rotate(float turnSpeed)
    {
        //This rotates the transfrom at the defined rate of turnspeed from Tank data around the defined axis of "Y" and multiplied the time between frames.
        transform.Rotate(turnSpeed * Vector3.up * Time.deltaTime);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    //References
    DefaultPojectile defaultProjectile;
    TankData data;
    public Transform EnemyBarrel;

    public float timeToShoot;



    // Start is called before the first frame update
    void Start()
    {
        //initial memory banking of component variables
        defaultProjectile = GetComponent<DefaultPojectile>();
        data = GetComponent<TankData>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void DefaultShoot()
    {
        //If the in game time is greater than or equal to the timeToShoot
        if (Time.time >= timeToShoot)
        {
            //Clone the Default Projectile compononet at the empty object for barrel's position
            DefaultPojectile clone = Instantiate(defaultProjectile, EnemyBarrel.position, EnemyBarrel.rotation);
            //Give it the shotSpeed defined in TankData
            clone.shotSpeed = data.shotSpeed;
            //Give it the damage from Tank Data
            clone.damage = data.damage;
            //timeToShoot is now the FireRate added to the current game time.
            timeToShoot = Time.time + data.FireRate;
            //Retrieve who had shot the projectile
            clone.instigator = data;
        }
    }
}

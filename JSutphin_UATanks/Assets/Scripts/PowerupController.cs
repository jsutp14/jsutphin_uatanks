﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    //References
    TankData data;


    //Variables
    public List<PowerUp>powerUps;


    // Start is called before the first frame update
    void Start()
    {
        //Initialize list
        powerUps = new List<PowerUp>();

        //Initial memory banking of reference
        data = GetComponent<TankData>();

    }

    // Update is called once per frame
    void Update()
    {
        // Create an List to hold our expired powerups
        List<PowerUp> expiredPowerups = new List<PowerUp>();

        // Loop through all the powers in the List
        foreach (PowerUp power in powerUps)
        {
            // Subtract from the timer
            power.duration -= Time.deltaTime;

            // Assemble a list of expired powerups
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }
        // Now that we've looked at every powerup in our list, use our list of expired powerups to remove the expired ones.
        foreach (PowerUp power in expiredPowerups)
        {
            power.OnDeactivate(data);
            powerUps.Remove(power);
        }
        // Since our expiredPowerups is local, it will *poof* into nothing when this function ends,
        // but let's clear it to learn how to empty a List
        expiredPowerups.Clear();
    }


    public void Add(PowerUp powerup)
    {
        powerup.OnActivate(data);
        powerUps.Add(powerup);

        // Only add the permanent ones to the list
        if (!powerup.isPermanent)
        {
            powerUps.Add(powerup);
        }
    }






}

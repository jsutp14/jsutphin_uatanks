﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinglePlayerSpawn : MonoBehaviour
{
    //References
    public GameObject PlayerTank;
    private Transform tf;
    private GameObject spawnedTank;
    private Buttons buttons;

    //Variables
    public float PlayerspawnDelay;
    private float nextSpawnTime;


    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        nextSpawnTime = Time.time + PlayerspawnDelay;
        spawnedTank = Instantiate(PlayerTank, tf.position, Quaternion.identity) as GameObject;
       
    }

    // Update is called once per frame
    void Update()
    {
        if (buttons.singlePlayer == true)
        {
            // If it is there is nothing spawns
            if (spawnedTank == null)
            {
                // And it is time to spawn
                if (Time.time > nextSpawnTime)
                {
                    // Spawn it and set the next time
                    spawnedTank = Instantiate(PlayerTank, tf.position, Quaternion.identity) as GameObject;
                    nextSpawnTime = Time.time + PlayerspawnDelay;
                }
            }

            else
            {
                // Otherwise, the object still exists, so postpone the spawn
                nextSpawnTime = Time.time + PlayerspawnDelay;
            }
        }
    }
}

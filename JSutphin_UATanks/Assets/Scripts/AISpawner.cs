﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISpawner : MonoBehaviour
{
    //References
    public GameObject AITank;
    private Transform tf;
    private GameObject spawnedTank;

    //Variables
    public float AIspawnDelay;
    private float nextSpawnTime;


    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        nextSpawnTime = Time.time + AIspawnDelay;
        spawnedTank = Instantiate(AITank, tf.position, Quaternion.identity) as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        // If it is there is nothing spawns
        if (spawnedTank == null)
        {
            // And it is time to spawn
            if (Time.time > nextSpawnTime)
            {
                // Spawn it and set the next time
                spawnedTank = Instantiate(AITank, tf.position, Quaternion.identity) as GameObject;
                nextSpawnTime = Time.time + AIspawnDelay;
            }
        }
        else
        {
            // Otherwise, the object still exists, so postpone the spawn
            nextSpawnTime = Time.time + AIspawnDelay;
        }
    }
}

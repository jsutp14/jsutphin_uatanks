﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPreception : MonoBehaviour
{
    //References
    SensoryData sense;
    SphereCollider AIPreceptionRadius;

    //Variables
    public float fieldOfView = 60.0F;
    private Transform player;

    //Booleans
    

    // Start is called before the first frame update
    void Start()
    {
        sense = GetComponent<SensoryData>();
        AIPreceptionRadius = GetComponent<SphereCollider>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Boolean function to read whether or not the player was heard
    public bool CanHear(GameObject target)
    {

        NoiseMaker noise;

        //Transform soundSource;

        //soundSource = noise.NoiseRadius;

        //Vector3 heardPlayer = soundSource.position;

        //target = noise.NoiseRadius;

        noise = target.GetComponent<NoiseMaker>();

        if(noise != null)
        {
            //if (noise.NoiseRadius.radius && AIPreceptionRadius.radius <= noise.NoiseRadius.radius + AIPreceptionRadius.radius)
            if(sense.shotRadius + sense.AISenseRadius <= sense.playerSenseRadius + sense.AISenseRadius)
            {
                return true;
            }
                
        }

        return false;

    }

    public bool CanSee(GameObject target)
    {
        //bool FOV = false;
        //bool LOS = false;

        //Player position
        Transform seen = target.GetComponent<Transform>();

            //Player position
            Vector3 playerPosition = seen.position;

            //This is accomplished by subtracting "destination minus origin", so that "origin plus vector equals destination."
            Vector3 enemyToPlayerVector = playerPosition - transform.position;


            //Find the angle of the enemies forward direction in relation to the player
            float angleToPlayer = Vector3.Angle(enemyToPlayerVector, transform.forward);

        //If that angle is less than our field of view
        if (angleToPlayer < fieldOfView)
        {
            //Raycast
            RaycastHit hitInfo;
            if (Physics.Raycast(seen.position, enemyToPlayerVector, out hitInfo, Mathf.Infinity))
            {

                //If the first object we hit is the player 
                if (hitInfo.collider.gameObject == target)
                {
                    return true;
                }
            }
        }
            return false;
    }

}

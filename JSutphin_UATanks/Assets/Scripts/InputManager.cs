﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
   //References
    CharacterController characterController;
    PlayerMovement motor;
    TankData data;
    PlayerAttack attack;
    DefaultPojectile defaultPojectile;
    Buttons buttons;
    SFXManager SFX;

    //Variables
    //public AudioClip OnShoot;

    //Movement Input using enumeration, and switch/case method
    public enum InputScheme { WASD, arrowKeys };
    public InputScheme input = InputScheme.WASD;
    // Start is called before the first frame update
    void Start()
    {
        //Initial memory banking of these components
        characterController = GetComponent<CharacterController>();
        motor = GetComponent<PlayerMovement>();
        data = GetComponent<TankData>();
        attack = GetComponent<PlayerAttack>();
    }

    // Update is called once per frame
    void Update()
    {
        //Switch for WASD or Arrow Key set up
        switch (input)
        {
            //First case which reads whether any of the keys for WASD are being pushed, and uses the correlating method to move
            case InputScheme.WASD:
                if (Input.GetKey(KeyCode.W))
                {
                    motor.Move(data.transform.forward);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    motor.Move(-data.transform.forward);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    motor.Rotate(-data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    motor.Rotate(data.turnSpeed);
                }
                //Added shooting statement in the enumeration
                if (Input.GetKey(KeyCode.Space))
                {
                    attack.DefaultShoot();
                    //Plays weapon shot sound
                    SFX.WeaponShot();

                }
                if (Input.GetKey(KeyCode.Tab))
                {
                    buttons.Menu();
                }
                break;
            //Second Case does thee same thing as the first, but with arrow keys
            case InputScheme.arrowKeys:
                if(Input.GetKey(KeyCode.UpArrow))
                {
                    motor.Move(data.transform.forward);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    motor.Move(-data.transform.forward);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    motor.Rotate(-data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    motor.Rotate(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.KeypadEnter))
                {
                    attack.DefaultShoot();
                    //Plays weapon shot sound
                    SFX.WeaponShot();

                }
                if (Input.GetKey(KeyCode.Tab))
                {
                    buttons.Menu();
                }
                break;
        }


    }
}

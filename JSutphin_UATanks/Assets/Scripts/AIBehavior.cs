﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBehavior : MonoBehaviour
{
    //References
    AIPreception AI_P;
    SensoryData sense;
    TankData data;
    Health health;
    private CharacterController characterController;

    public Transform[] patrolPoints;
    private int randomPoint;
    private Transform player;

    //Variables
    public float waitTime;
    public float startWaitTime;
    public float lostPlayer;
    public float AISpeed;
    public float nearDistance;
    public float stoppingDistance;
    public float fleeDistance;
    GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        //Initial memory banking of components
        AI_P = GetComponent<AIPreception>();
        sense = GetComponent<SensoryData>();
        data = GetComponent<TankData>();
        health = GetComponent<Health>();
        //randomPoint is now defined by the number of patrol points within the array which it will randomly choose 1 at a time.
        randomPoint = Random.Range(0, patrolPoints.Length);
        waitTime = startWaitTime;
        //Player is now the object with the player tag
        player = GameObject.FindGameObjectWithTag("Player").transform;


    }

    // Update is called once per frame
    void Update()
    {
        //*****ADD FINITE STATE MACHINE****


        if(target == null)
        {
            target = player.gameObject;
        }

        //If the player is heard the AI will investigate
        if (AI_P.CanHear(target) == true)
        {
            Investigate();
        }
        //If the player is seen the AI will begin to chase the player
        if (AI_P.CanSee(target) == true)
        {
            Chase();
        }
        //If the bot is one shot away from death it will flee
        if (data.curHealth <= 10)
        {
            Flee();
        }
        //If none of the above than the bot will patrol
        else
        {
            Patrol();
        }
    }

    private void Investigate()
    {
        //Will move toward the players position to investigate
        transform.position = Vector3.MoveTowards(transform.position, player.position, AISpeed * Time.deltaTime);
    }

    private void Chase()
    {
        //Code to keep AI at a distance from the player while attacking
        if(Vector3.Distance(transform.position, player.position) < nearDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, -AISpeed * Time.deltaTime);
        }
        else if(Vector3.Distance(transform.position, player.position) > stoppingDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, AISpeed * Time.deltaTime);
        }
        else if(Vector3.Distance(transform.position, player.position) < stoppingDistance && Vector3.Distance(transform.position, player.position) > nearDistance)
        {
            transform.position = this.transform.position;
        }
    }

    private void Flee()
    {
        //the fleeDistance variable is what differenciates this code from the chase code. The distance is just further from the player than the keeping distance code in the chase
        if (Vector3.Distance(transform.position, player.position) < fleeDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, -AISpeed * Time.deltaTime);
        }
    }
    private void Patrol()
    {
        //This moves toward the patrol points given from the array in a random order
        transform.position = Vector3.MoveTowards(transform.position, patrolPoints[randomPoint].position,  AISpeed * Time.deltaTime);

        //if it is within 0.2f of the point than it will have made it to the area it is intended to make it to
        if(Vector3.Distance(transform.position, patrolPoints[randomPoint].position) < 0.2f)
        {
            //after waiting the specified time the AI will move to the next specified patrol point
            if(waitTime <= 0)
            {
                randomPoint = Random.Range(0, patrolPoints.Length);
                waitTime = startWaitTime;
            }
            else
            {
                waitTime = waitTime - Time.deltaTime;
            }
        }
    }

}

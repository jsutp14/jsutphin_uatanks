﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoad : MonoBehaviour
{
    //References
    GameManager instance;


    //Variables
    string currentGame;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Save()
    {
        PlayerPrefs.SetString("Last Save", currentGame);
        PlayerPrefs.Save();
    }

    public void Load()
    {
        currentGame = PlayerPrefs.GetString("Last Save");
    }

}

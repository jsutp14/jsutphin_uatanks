﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SFXManager : MonoBehaviour
{
    //References
    public AudioSource SFX;
    public float SFXVolume;

    public AudioClip Click;
    

    public static SFXManager sfxInstance;
    public Slider backgroundSlider;
    //public float backgroundFloat;


    //Variables
    public AudioSource Shot;
    public AudioSource death;


    private void Awake()
    {
        //Singleton method for Menu to Background music so that it will continue to play
        if (sfxInstance != null && sfxInstance != this)
        {
            Destroy(this.gameObject);
            return;

        }
        //Music audio volume is correlated directly to the slider volume
        SFX.volume = SFXVolume;
        //SFXVolume = backgroundSlider.value;

        sfxInstance = this;
        DontDestroyOnLoad(this);
    }

    public void WeaponShot()
    {
        Shot.Play();
    }

    public void DeathSound()
    {
        death.Play();
    }

    //public void UpdateSound()
    //{


    //    //directly links the background volume to the slider value
    //    SFX.volume = backgroundSlider.value;
    //}
}


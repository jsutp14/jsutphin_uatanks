﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundMusic : MonoBehaviour
{
    //Singlton variable(only one background instance of this game)
    public static BackgroundMusic BgInstance;
    public SFXManager SFX;

    ////Variables/references to link to slider in options menu
    //private static string FirstPlay = "FirstPlay";
    //private static string BackgroundPref = "BackgroundPref";
    //private int firstPlayInt;



    // Start is called before the first frame update
    private void Awake()
    {
        //Singleton method for Menu to Background music so that it will continue to play
        if (BgInstance != null && BgInstance != this)
        {
            Destroy(this.gameObject);
            return;

        }

        BgInstance = this;
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        ////Using PlayerPrefs to save the sound from exit to entry
        //firstPlayInt = PlayerPrefs.GetInt(FirstPlay);

        //if (firstPlayInt == 0)
        //{
        //    backgroundSlider.value = backgroundFloat;
        //    PlayerPrefs.SetFloat(BackgroundPref, backgroundFloat);
        //}
        //else
        //{

        //}
    }

}
